﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	float sens;

	// Use this for initialization
	void Start () {
		sens = 2f;	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			Vector3 rot = transform.rotation.eulerAngles;
			rot.y -= sens;
			transform.rotation = Quaternion.Euler(rot.x,rot.y,rot.z);

		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			Vector3 rot = transform.rotation.eulerAngles;
			rot.y += sens;
			transform.rotation = Quaternion.Euler(rot.x,rot.y,rot.z);
		}
	}
}
