﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {


	// Move 
	void move(Vector3 dest, float v) {

		if (transform.position != dest) {

			if (Vector3.Distance (transform.position, dest) > 1) { 
				
				Vector3 dir_unnormalized = dest - transform.position;
				Vector3 dir = dir_unnormalized.normalized;
				transform.position += Time.deltaTime * dir * v; 

			}

		}
	}



	// Use this for initialization
	void Start () {
		
	
	}


	
	// Update is called once per frame
	void Update () {

		// move to Position Vector3 x,y,z, with speed s
		Vector3 dest = new Vector3(5f, 5f, 50f);
		float velocity = 2;

		move (dest, velocity);

	}
}
