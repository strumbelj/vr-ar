﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingObject : MonoBehaviour {

	private float speed = 0.05f;
	private GameObject player;

	public void Start(){
		player = GameObject.FindGameObjectWithTag("player");
		GameObject temp = GameObject.FindGameObjectWithTag("containerObjects");
		speed = temp.GetComponent<ContainerObjects>().speedFlying;
	}

	public void Update () {
		transform.position = Vector3.Lerp(transform.position,player.transform.position,speed);
	}
}
