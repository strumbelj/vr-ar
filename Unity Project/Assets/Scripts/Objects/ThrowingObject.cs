﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingObject : MonoBehaviour {

	public GameObject flyingObject;

	private float speed = 2;
	private float timeLast;

	void Start () {
		GameObject temp = GameObject.FindGameObjectWithTag("containerObjects");
		speed = temp.GetComponent<ContainerObjects>().speedThrowing;
		timeLast = Time.time;
	}

	void Update () {
		if (timeLast + speed <= Time.time) {
			GameObject temp = Instantiate (flyingObject);
			temp.transform.position = transform.position;
			timeLast = Time.time;
		}
	}
}
