﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageHandler : MonoBehaviour {

	public float damageDone = 5f;
	public GameObject particles;

	//If hit, destroy 
	void OnCollisionEnter(Collision collision)
	{
		GameObject obj = collision.gameObject;

		if (obj.tag == "sword") 
		{
			Instantiate(particles, transform);
			Debug.Log("Object destroyed!");
			Destroy(this);
		}
	}

	public float GetDamage()
	{
		return damageDone;
	}
}
