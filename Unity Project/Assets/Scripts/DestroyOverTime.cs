﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverTime : MonoBehaviour {

	public float time = 3;

	private float timeStart;

	void Start(){
		timeStart = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (timeStart + time <= Time.time) {
			Destroy(this);
		}
	}
}
