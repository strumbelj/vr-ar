﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitController : MonoBehaviour {

	public GameObject blood;

	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "flyingObject"){
			GameObject temp = Instantiate(blood);
			temp.transform.position = transform.position;
			Destroy(collision.gameObject);
		}
	}
}
