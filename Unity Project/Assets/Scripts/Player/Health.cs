﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
	
	public float health = 100f;

	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("player");
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0)
		{
			Debug.Log("Dead");
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		GameObject obj = collision.gameObject;

		if (obj.tag == "flyobj") 
		{
			float dmg = obj.GetComponent<DamageHandler>().GetDamage();
			health -= dmg;
			Debug.Log("Got Damage: " + dmg);
		}
	}
}
